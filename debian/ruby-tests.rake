#-*- mode: ruby; coding: utf-8 -*-
#
#require 'gem2deb/rake/testtask'

require 'rspec/core/rake_task'
RSpec::Core::RakeTask.new(:test)
task :default =>  :test


#Gem2Deb::Rake::TestTask.new do |t|
namespace :test do
  if not File.exist?("./spec/fixtures/modules/augeas_core") then
    FileUtils.mkdir_p "./spec/fixtures/modules/augeas_core"
    FileUtils.symlink("/usr/share/puppet/vendor_modules/augeas_core/lib", "./spec/fixtures/modules/augeas_core")
  end
  if not File.exist?("./spec/fixtures/modules/stdlib") then
    FileUtils.mkdir_p "./spec/fixtures/modules/stdlib"
    FileUtils.symlink("/usr/share/puppet/modules.available/puppetlabs-stdlib/lib", "./spec/fixtures/modules/stdlib")
  end
end
